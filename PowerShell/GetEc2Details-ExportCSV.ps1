# Get EC2 basic details and export to a CSV.
# Need appropriate permissions to read EC2 details, set in the profile below. Replace with your Profile name.
#Set-AWSCredential -ProfileName MyExampleReadOnlyAccessRole
$inputcsv = Import-CSV input.csv
$ec2List = Get-EC2Instance -Filter @{'name'='instance-state-name';'values'='running'}


foreach ($i in $CSV){


#run against AWS APIs
$TerminationProtection = (Get-EC2InstanceAttribute -InstanceId $i.instanceid -Attribute disableApiTermination |Select-Object DisableApiTermination | out-string)

 
$SortEC2 = $ec2List.Instances | Where-Object {($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name )}

#hash table to store all the properties required for the CSV
$ec2DetailsList = $SortEC2| ForEach-Object {
    $properties = [ordered]@{
    Name         = ($_ | Select-Object -ExpandProperty tags | Where-Object -Property Key -eq Name).value
    InstanceID    = $_.InstanceId
    Platform = $_.Platform
    TerminationProtection = (Get-EC2InstanceAttribute -InstanceId $i.instanceid -Attribute disableApiTermination |Select-Object DisableApiTermination)
    }
    New-Object -TypeName PSObject -Property $properties
}
}
$ec2DetailsList | Sort-Object -Property Name | Export-Csv -Path output.csv -NoTypeInformation
